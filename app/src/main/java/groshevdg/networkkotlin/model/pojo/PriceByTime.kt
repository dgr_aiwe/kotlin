package groshevdg.networkkotlin.model.pojo

import com.google.gson.annotations.SerializedName

open class PriceByTime() {
    @SerializedName("Time Series (60min)") var priceByHours: Map<String, Price>? = null
    @SerializedName("Time Series (1min)") var priceByMinutes: Map<String, Price>? = null
    @SerializedName("Weekly Time Series") var priceByWeeks: Map<String, Price>? = null
    @SerializedName("Monthly Time Series") var priceByMonths: Map<String, Price>? = null
}