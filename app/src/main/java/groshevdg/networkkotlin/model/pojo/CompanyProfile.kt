package groshevdg.networkkotlin.model.pojo

import com.google.gson.annotations.SerializedName

class CompanyProfile(@SerializedName("price") val price: String,
    @SerializedName("beta") val beta: String,
    @SerializedName("volAvg") val volAvg: String,
    @SerializedName("mktCap") val mktCap: String,
    @SerializedName("lastDiv") val lastDiv: String,
    @SerializedName("range") val range: String,
    @SerializedName("changes") val changes: String,
    @SerializedName("changesPercentage") val changesPercentage: String,
    @SerializedName("companyName") val companyName: String,
    @SerializedName("exchange") val exchange: String,
    @SerializedName("industry") val industry: String,
    @SerializedName("website") val website: String,
    @SerializedName("description") val description: String,
    @SerializedName("ceo") val ceo: String,
    @SerializedName("sector") val sector: String,
    @SerializedName("image") val image: String)