package groshevdg.networkkotlin.model.pojo

import com.google.gson.annotations.SerializedName

class StocksList( @SerializedName("companies" ) val stocks: List<Stock>,
    @SerializedName("next_page")var nextPage: String? = null)