package groshevdg.networkkotlin.model.pojo

import com.google.gson.annotations.SerializedName

class Stock(@SerializedName("name") val companyName: String,
    @SerializedName("ticker") val ticker: String)