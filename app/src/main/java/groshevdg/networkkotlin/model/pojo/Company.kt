package groshevdg.networkkotlin.model.pojo

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import groshevdg.networkkotlin.ui.company.adapter.item.RecyclerItem
import java.io.Serializable

@Entity
class Company(@SerializedName("profile")
              @Embedded
              val profile: CompanyProfile,

              @SerializedName("symbol")
              @PrimaryKey
              val symbol: String) : RecyclerItem, Serializable