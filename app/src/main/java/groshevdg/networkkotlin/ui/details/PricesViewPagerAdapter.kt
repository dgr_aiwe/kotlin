package groshevdg.networkkotlin.ui.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import groshevdg.networkkotlin.R
import kotlinx.android.synthetic.main.item_stock_price_detalize.view.*


class PricesViewPagerAdapter : PagerAdapter() {
    private val points = ArrayList<LineGraphSeries<DataPoint>>()

    fun setData(list: List<LineGraphSeries<DataPoint>>) {
        points.addAll(list)
        this.notifyDataSetChanged()
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return points.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.item_stock_price_detalize, container, false) as ViewGroup

        when (position) {
            0 -> {
                view.ispdTitleTimeTextView.text = "Minutes"
                view.ispdGraphic.addSeries(points[0])
            }
            1 -> {
                view.ispdTitleTimeTextView.text = "Hours"
                view.ispdGraphic.addSeries(points[1])
            }
            2 -> {
                view.ispdTitleTimeTextView.text = "Weeks"
                view.ispdGraphic.addSeries(points[2])
            }
            3 -> {
                view.ispdTitleTimeTextView.text = "Months"
                view.ispdGraphic.addSeries(points[3])
            }
        }
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}