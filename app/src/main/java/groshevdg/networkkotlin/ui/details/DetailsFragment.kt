package groshevdg.networkkotlin.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import groshevdg.networkkotlin.R
import groshevdg.networkkotlin.di.factory.ViewModelFactory
import groshevdg.networkkotlin.model.pojo.*
import groshevdg.networkkotlin.ui.company.MainActivity
import groshevdg.networkkotlin.ui.company.adapter.viewHolder.loadImage
import kotlinx.android.synthetic.main.fragment_details.view.*
import kotlinx.coroutines.*
import javax.inject.Inject

class DetailsFragment : Fragment() {

    @Inject lateinit var factory: ViewModelFactory
    private lateinit var viewModel: DetailsViewModel
    private lateinit var company: Company
    private lateinit var fragmentView: View
    private lateinit var networkJob: Job
    private val adapter: PricesViewPagerAdapter = PricesViewPagerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).activityComponent.plusFragmentComponent().inject(this)
        viewModel = ViewModelProvider(this, factory).get(DetailsViewModel::class.java)
        company = arguments?.getSerializable("selectedCompany") as Company
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_details, container, false)

        fragmentView.fdToolbar.title = "Details"
        fragmentView.fdToolbar.setNavigationIcon(R.drawable.ic_back)
        fragmentView.fdToolbar.setNavigationOnClickListener { v -> activity?.onBackPressed() }

        fragmentView.fdViewPagerGraphics.adapter = adapter

        fragmentView.fdDescriptionTextView.text = company.profile.description
        fragmentView.fdCompanyNameTextView.text = company.profile.companyName
        fragmentView.fdLogoImageView.loadImage(context!!, company.profile.image, fragmentView.fdLogoImageView)

        return fragmentView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        networkJob = GlobalScope.launch(Dispatchers.Main) {
            viewModel.getPrices(company.symbol).observe(viewLifecycleOwner, observer) }
    }

    private val observer = Observer<List<PriceByTime>> {
        val coordinates = getCoordinates(it)
        adapter.setData(coordinates)
        fragmentView.fdProgress.visibility = View.GONE
    }

    override fun onStop() {
        super.onStop()
        networkJob.cancel()
    }

    private fun getCoordinates(list: List<PriceByTime>) : List<LineGraphSeries<DataPoint>> {
        val priceByMinutes = list[0].priceByMinutes ?: emptyMap()
        val pointsForMinutes = pointsByTime(priceByMinutes)

        val priceByHours = list[1].priceByHours ?: emptyMap()
        val pointsForHours = pointsByTime(priceByHours)

        val priceByWeeks = list[2].priceByWeeks ?: emptyMap()
        val pointsForWeeks = pointsByTime(priceByWeeks)

        val priceByMonths = list[3].priceByMonths ?: emptyMap()
        val pointsForMonths = pointsByTime(priceByMonths)

        val coordinates: MutableList<LineGraphSeries<DataPoint>> = ArrayList()

        coordinates.add(LineGraphSeries(pointsForMinutes))
        coordinates.add(LineGraphSeries(pointsForHours))
        coordinates.add(LineGraphSeries(pointsForWeeks))
        coordinates.add(LineGraphSeries(pointsForMonths))

        return coordinates
    }

    private fun pointsByTime(price: Map<String, Price>): Array<DataPoint> {
        var count = 0
        val points = mutableListOf<DataPoint>()
        for (value in price) {
            if (count == 8) break

            points.add(count, DataPoint((count.toDouble()), (value.value.high.toDouble())))
            count++
        }
        return points.toTypedArray()
    }
}
