package groshevdg.networkkotlin.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import groshevdg.networkkotlin.data.repository.StocksRepository
import groshevdg.networkkotlin.model.pojo.PriceByTime
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class DetailsViewModel @Inject constructor(private val repository: StocksRepository) : ViewModel() {
    suspend fun getPrices(symbol: String) : LiveData<List<PriceByTime>> {
        return repository.loadDetails(symbol).asLiveData(Dispatchers.Main, 0)
    }
}