package groshevdg.networkkotlin.ui.company

import android.util.Log
import androidx.lifecycle.*
import groshevdg.networkkotlin.config.Const
import groshevdg.networkkotlin.data.repository.StocksRepository
import groshevdg.networkkotlin.model.pojo.Company
import groshevdg.networkkotlin.model.pojo.Resource
import groshevdg.networkkotlin.ui.company.adapter.item.ProgressItem
import groshevdg.networkkotlin.ui.company.adapter.item.RecyclerItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class CompaniesViewModel @Inject constructor(val repository: StocksRepository) : ViewModel() {
     var allowLoading = true
     private val listOfCompanies = MutableLiveData<List<RecyclerItem>>()
     val listItemsLiveData: LiveData<List<RecyclerItem>> = listOfCompanies
     var offset = 0

     fun getCompanies(quantity: Int) {
          viewModelScope.launch {
              val resource = repository.getCompanies(quantity, offset)
              if (resource.status == Resource.Status.SUCCESS) {
                  sortList(resource.data!!)
                  listOfCompanies.postValue(resource.data)
                  Log.d("Debug", resource.data!!.size.toString())
              }
          }
     }


    //Rewrite
    private fun sortList(list: List<Company>) {
        Collections.sort(list, kotlin.Comparator { o1, o2 ->
            when {
                o1.symbol[0] <= o2.symbol[0] -> -1
                o1.symbol[0] == o2.symbol[0] -> 0
                else -> 1
            }
        })
    }

    fun getMoreCompanies(quantity: Int) {
        addProgressToList()
        getCompanies(quantity)
     }

    fun refresh() {
          viewModelScope.launch { val resource = repository.getCompanies(Const.LOAD_FIRST_TIME, 0)
          if (resource.status == Resource.Status.SUCCESS) {
              listOfCompanies.postValue(resource.data)
          }}
     }

    private fun addProgressToList() {
        val list: MutableList<RecyclerItem> = listItemsLiveData.value as MutableList<RecyclerItem>
        list.add(ProgressItem)
        listOfCompanies.value = list
    }
}