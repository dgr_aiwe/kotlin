package groshevdg.networkkotlin.ui.company.adapter.viewHolder

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import groshevdg.networkkotlin.R
import groshevdg.networkkotlin.model.pojo.Company
import groshevdg.networkkotlin.ui.company.adapter.CompanyWasSelectedListener

class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val logo: ImageView = itemView.findViewById(R.id.isLogoImageView)
    private val companyName: TextView = itemView.findViewById(R.id.isCompanyNameTextView)
    private val percent: TextView = itemView.findViewById(R.id.isPercentOfChangeTextView)
    private val price: TextView = itemView.findViewById(R.id.isStockCostTextView)

    fun bind(company: Company, listener: CompanyWasSelectedListener) {
        itemView.setOnClickListener {v -> listener.selected(company)}
        companyName.text = company.profile.companyName
        percent.text = company.profile.changesPercentage
        price.text = company.profile.price
        logo.loadImage(itemView.context, company.profile.image, logo)
    }
}

fun ImageView.loadImage(context: Context, url: String, place: ImageView) {
    Glide.with(context)
        .load(url)
        .into(place)
}