package groshevdg.networkkotlin.ui.company

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import groshevdg.networkkotlin.App
import groshevdg.networkkotlin.R
import groshevdg.networkkotlin.di.component.ActivityComponent
import groshevdg.networkkotlin.di.component.DaggerActivityComponent
import groshevdg.networkkotlin.di.module.NavModule
import groshevdg.networkkotlin.ui.Navigator
import javax.inject.Inject

class MainActivity @Inject constructor() : AppCompatActivity() {

    @Inject lateinit var navigator: Navigator
    lateinit var activityComponent: ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        activityComponent = DaggerActivityComponent.builder().appComponent(App.application.appComponent)
            .navModule(NavModule(this)).build()

        activityComponent.inject(this)
        navigator.openStockList()
    }
}
