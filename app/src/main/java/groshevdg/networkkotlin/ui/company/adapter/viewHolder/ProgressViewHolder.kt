package groshevdg.networkkotlin.ui.company.adapter.viewHolder

import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import groshevdg.networkkotlin.R

class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val progressBar: ProgressBar = itemView.findViewById(R.id.ipItemProgressBar);
}