package groshevdg.networkkotlin.ui.company

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import groshevdg.networkkotlin.R
import groshevdg.networkkotlin.config.Const
import groshevdg.networkkotlin.di.factory.ViewModelFactory
import groshevdg.networkkotlin.model.pojo.Company
import groshevdg.networkkotlin.ui.Navigator
import groshevdg.networkkotlin.ui.company.adapter.CompanyWasSelectedListener
import groshevdg.networkkotlin.ui.company.adapter.StocksRecyclerAdapter
import groshevdg.networkkotlin.ui.company.adapter.diffUtils.CompanyDiffUtils
import groshevdg.networkkotlin.ui.company.adapter.item.RecyclerItem
import kotlinx.android.synthetic.main.fragment_stock_list.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class CompaniesFragment : Fragment(), CompanyWasSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    @Inject lateinit var factory: ViewModelFactory
    @Inject lateinit var navigator: Navigator
    private lateinit var viewModel: CompaniesViewModel
    private val adapter: StocksRecyclerAdapter = StocksRecyclerAdapter()
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var fragmentView: View
    private val ITEMS_TO_END_LIST = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).activityComponent.plusFragmentComponent().inject(this)
        viewModel = ViewModelProvider(this, factory).get(CompaniesViewModel::class.java)
        adapter.setSelectedItemListener(this)
        viewModel.getCompanies(Const.LOAD_FIRST_TIME)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_stock_list, container, false)
        fragmentView.fslStocksRecycleView.adapter = adapter
        layoutManager = LinearLayoutManager(this.context)
        fragmentView.fslStocksRecycleView.layoutManager = layoutManager
        fragmentView.fslSwipeRefreshLayout.setOnRefreshListener(this)
        return fragmentView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.listItemsLiveData.observe(viewLifecycleOwner, Observer { items ->
                fragmentView.fslProgress.visibility = View.GONE
                viewModel.allowLoading = items.size % Const.LOAD_NEXT_TIME == 0
                fragmentView.fslSwipeRefreshLayout.isRefreshing = false

                adapter.submitList(items)
                //viewModel.offset = adapter.itemCount
        })
    }


    override fun onResume() {
        super.onResume()
        fragmentView.fslStocksRecycleView.addOnScrollListener(scrollListener)
    }

    override fun onPause() {
        super.onPause()
        fragmentView.fslStocksRecycleView.removeOnScrollListener(scrollListener)
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (viewModel.allowLoading) {
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                val sizeOfDataList = adapter.itemCount

                if (sizeOfDataList - lastVisiblePosition <= ITEMS_TO_END_LIST) {
                    viewModel.allowLoading = false
                    viewModel.getMoreCompanies(adapter.itemCount + Const.LOAD_NEXT_TIME)
                }
            }
            super.onScrolled(recyclerView, dx, dy)
        }
    }

    override fun selected(company: Company) {
        val bundle = Bundle()
        bundle.putSerializable("selectedCompany", company)
        navigator.openDetailsFragment(bundle)
    }

    override fun onRefresh() {
        viewModel.refresh()
    }
}