package groshevdg.networkkotlin.ui.company.adapter

import groshevdg.networkkotlin.model.pojo.Company

interface CompanyWasSelectedListener {
    fun selected(company: Company)
}