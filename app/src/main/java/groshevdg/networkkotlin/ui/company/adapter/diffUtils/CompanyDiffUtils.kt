package groshevdg.networkkotlin.ui.company.adapter.diffUtils

import androidx.recyclerview.widget.DiffUtil
import groshevdg.networkkotlin.model.pojo.Company
import groshevdg.networkkotlin.ui.company.adapter.item.RecyclerItem

class CompanyDiffUtils: DiffUtil.ItemCallback<RecyclerItem>() {

    override fun areItemsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean {
       return if (oldItem is Company && newItem is Company) {
           oldItem.symbol == newItem.symbol
       }
        else false
    }

    override fun areContentsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean {
        return if (oldItem is Company && newItem is Company) {
            oldItem.profile.price == newItem.profile.price && oldItem.profile.changesPercentage ==
                    newItem.profile.changesPercentage
        }
        else false
    }
}