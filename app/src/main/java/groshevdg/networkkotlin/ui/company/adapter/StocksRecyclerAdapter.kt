package groshevdg.networkkotlin.ui.company.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import groshevdg.networkkotlin.R
import groshevdg.networkkotlin.model.pojo.Company
import groshevdg.networkkotlin.ui.company.adapter.diffUtils.CompanyDiffUtils
import groshevdg.networkkotlin.ui.company.adapter.item.RecyclerItem
import groshevdg.networkkotlin.ui.company.adapter.viewHolder.DataViewHolder
import groshevdg.networkkotlin.ui.company.adapter.viewHolder.ProgressViewHolder

class StocksRecyclerAdapter : ListAdapter<RecyclerItem, RecyclerView.ViewHolder>(CompanyDiffUtils()) {
    private val ITEM_DATA: Int = 0
    private val ITEM_PROGRESS: Int = 1
    private lateinit var listener: CompanyWasSelectedListener

    fun setSelectedItemListener(listener: CompanyWasSelectedListener) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is Company -> ITEM_DATA
            else -> ITEM_PROGRESS
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_DATA -> DataViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_stock, parent, false))

            else -> ProgressViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_progress, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DataViewHolder) {
             holder.bind((getItem(position) as Company), listener)
        }
    }
}