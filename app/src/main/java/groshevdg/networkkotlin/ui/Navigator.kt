package groshevdg.networkkotlin.ui

import android.os.Bundle
import groshevdg.networkkotlin.ui.company.CompaniesFragment
import groshevdg.networkkotlin.ui.company.MainActivity
import groshevdg.networkkotlin.ui.details.DetailsFragment
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class Navigator @Inject constructor(private val activity: MainActivity) {

    fun openStockList() {
       activity.supportFragmentManager.beginTransaction()
           .replace(activity.amTopContainer.id,
               CompaniesFragment()
           )
           .commit()
    }

    fun openDetailsFragment(bundle: Bundle) {
        val detailsFragment = DetailsFragment()
        detailsFragment.arguments = bundle
        activity.supportFragmentManager.beginTransaction()
            .replace(activity.amTopContainer.id, detailsFragment)
            .addToBackStack("detailsFragment")
            .commit()
    }
}