package groshevdg.networkkotlin

import android.app.Application
import groshevdg.networkkotlin.di.component.AppComponent
import groshevdg.networkkotlin.di.component.DaggerAppComponent
import groshevdg.networkkotlin.di.module.ContextModule
import groshevdg.networkkotlin.di.module.DatabaseModule
import groshevdg.networkkotlin.di.module.RetrofitModule
import javax.inject.Inject

class App : Application() {

    @Inject
    lateinit var appComponent: AppComponent

    companion object {
        lateinit var application: App
    }

    override fun onCreate() {
        super.onCreate()

        application = this
        appComponent = DaggerAppComponent.builder()
            .contextModule(ContextModule(applicationContext))
            .databaseModule(DatabaseModule())
            .retrofitModule(RetrofitModule())
            .build()
    }
}