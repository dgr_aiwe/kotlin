package groshevdg.networkkotlin.data.db.database

import androidx.room.Database
import androidx.room.RoomDatabase
import groshevdg.networkkotlin.data.db.dao.CompanyDao
import groshevdg.networkkotlin.model.pojo.Company

@Database(entities = [Company::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun companyDao() : CompanyDao
}