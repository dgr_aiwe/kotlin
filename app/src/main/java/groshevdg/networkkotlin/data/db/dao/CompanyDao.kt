package groshevdg.networkkotlin.data.db.dao

import androidx.room.*
import groshevdg.networkkotlin.model.pojo.Company
import kotlinx.coroutines.flow.Flow

@Dao
interface CompanyDao {

    @Query("select * from Company limit :limit offset :offset")
    fun getCompanies(limit: Int, offset: Int) : List<Company>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCompanies(list: List<Company>)
}