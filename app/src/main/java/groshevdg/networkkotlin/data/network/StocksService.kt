package groshevdg.networkkotlin.data.network

import groshevdg.networkkotlin.model.pojo.Company
import groshevdg.networkkotlin.model.pojo.PriceByTime
import groshevdg.networkkotlin.model.pojo.StocksList
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface StocksService {
    @GET("companies")
    fun loadStocks(@Query("api_key") api_key: String,
                   @Query("page_size") page_size: Int,
                   @Query("next_page") next_page: String?) : Deferred<StocksList>

    @GET
    fun getDetails(@Url url: String): Deferred<Company>

    @GET
    fun getPrice(@Url url: String,
                 @Query("function") time: String,
                 @Query("symbol") symbol: String,
                 @Query("interval") interval: String?,
                 @Query("apikey") api_key: String?): Deferred<PriceByTime>
}