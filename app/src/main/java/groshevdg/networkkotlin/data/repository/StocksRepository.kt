package groshevdg.networkkotlin.data.repository

import groshevdg.networkkotlin.data.db.database.AppDatabase
import groshevdg.networkkotlin.data.network.StocksService
import groshevdg.networkkotlin.model.pojo.Company
import groshevdg.networkkotlin.model.pojo.PriceByTime
import groshevdg.networkkotlin.model.pojo.Resource
import groshevdg.networkkotlin.model.pojo.StocksList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class StocksRepository @Inject constructor(private val api: StocksService, private val database: AppDatabase) {
    private val STOCKS_API_KEY: String = "OjkyYmU3NzI1ZjA3NTg0NGRjNjE3YzA5ZTgwN2IwZWZl"
    private val PRICE_API_KEY: String = "1M0ILJ2KQ08IFMK"
    private val DETAILS_URL: String = "https://financialmodelingprep.com/api/v3/company/profile/"
    private val PRICE_URL: String = "https://www.alphavantage.co/query"
    private val ONE_MINUTE = "1min"
    private val SIXTY_MINUTES = "60min"
    private val FOR_MONTH = "TIME_SERIES_MONTHLY"
    private val FOR_DAY = "TIME_SERIES_INTRADAY"
    private val FOR_WEEK = "TIME_SERIES_WEEKLY"
    private var nextPageReference: String? = ""
    private var fromDB = true

    suspend fun getCompanies(count: Int, offset: Int) : Resource<List<Company>> {
        return if (fromDB) {
            fromDB = false
            Resource.success(getFromDB(count, offset))
        } else {
            loadCompaniesAndSaveInBD(count)
        }
    }

    suspend fun loadDetails(symbol: String) : Flow<List<PriceByTime>> {
        val prices: MutableList<PriceByTime> = ArrayList()

        val priceByMinutes = api.getPrice(PRICE_URL, FOR_DAY, symbol, ONE_MINUTE, PRICE_API_KEY).await()
        val priceByHours = api.getPrice(PRICE_URL, FOR_DAY, symbol, SIXTY_MINUTES, PRICE_API_KEY).await()
        val priceByWeeks = api.getPrice(PRICE_URL, FOR_WEEK, symbol, null, PRICE_API_KEY).await()
        val priceByMonths = api.getPrice(PRICE_URL, FOR_MONTH, symbol, null, PRICE_API_KEY).await()

        prices.add(priceByMinutes)
        prices.add(priceByHours)
        prices.add(priceByWeeks)
        prices.add(priceByMonths)

        return flow { emit(prices) }
    }

    private suspend fun getCompanies(stocks: StocksList) : List<Company> {
        val companies: MutableList<Company> = ArrayList()
        for (stock in stocks.stocks) {
            val company = api.getDetails(DETAILS_URL + stock.ticker).await()
            companies.add(company)
        }
        return companies
    }

    private suspend fun loadCompaniesAndSaveInBD(pageSize: Int) : Resource<List<Company>> {
        try {
            val stocks: StocksList = api.loadStocks(STOCKS_API_KEY, pageSize, null).await()
            nextPageReference = stocks.nextPage

            val companyList = getCompanies(stocks)
            saveInBD(companyList)

            return Resource.success(companyList)
        }
        catch (e: Exception) {
            return Resource.error(e.cause!!, null)
        }
    }

    private suspend fun getFromDB(count: Int, offset: Int) : List<Company> {
        var list: List<Company>? = null
        GlobalScope.async { list = database.companyDao().getCompanies(count, offset) }.await()
        return list!!
    }

    private fun saveInBD(companyList: List<Company>) {
        GlobalScope.launch(Dispatchers.IO) { database.companyDao().insertCompanies(companyList) }
    }
}