package groshevdg.networkkotlin.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import groshevdg.networkkotlin.di.factory.ViewModelKey
import groshevdg.networkkotlin.ui.company.CompaniesViewModel
import groshevdg.networkkotlin.ui.details.DetailsViewModel

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CompaniesViewModel::class)
    internal abstract fun bindCompanyViewModel(companiesViewModel: CompaniesViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    internal abstract fun bindDetailsViewModel(detailsViewModel: DetailsViewModel) : ViewModel
}