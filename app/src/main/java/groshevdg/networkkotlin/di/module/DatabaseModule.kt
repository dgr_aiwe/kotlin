package groshevdg.networkkotlin.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import groshevdg.networkkotlin.data.db.database.AppDatabase
import javax.inject.Singleton

@Module(includes = [ContextModule::class])
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context) : AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "appDatabase").build()
    }
}