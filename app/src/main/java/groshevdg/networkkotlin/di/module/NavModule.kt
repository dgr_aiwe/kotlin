package groshevdg.networkkotlin.di.module

import dagger.Module
import dagger.Provides
import groshevdg.networkkotlin.di.scope.ActivityScope
import groshevdg.networkkotlin.ui.company.MainActivity
import groshevdg.networkkotlin.ui.Navigator

@Module
class NavModule(val mainActivity: MainActivity) {

    @Provides
    @ActivityScope
    fun provideNavigator() : Navigator {
        return Navigator(mainActivity)
    }
}