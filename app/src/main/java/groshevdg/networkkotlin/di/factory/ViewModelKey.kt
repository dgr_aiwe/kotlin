package groshevdg.networkkotlin.di.factory

import androidx.lifecycle.ViewModel
import dagger.MapKey
import groshevdg.networkkotlin.di.scope.ActivityScope
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
@ActivityScope
annotation class ViewModelKey(val value: KClass<out ViewModel>)