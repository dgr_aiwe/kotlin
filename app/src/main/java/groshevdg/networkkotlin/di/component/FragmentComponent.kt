package groshevdg.networkkotlin.di.component

import dagger.Subcomponent
import groshevdg.networkkotlin.di.module.FragmentModule
import groshevdg.networkkotlin.di.scope.FragmentScope
import groshevdg.networkkotlin.ui.company.CompaniesFragment
import groshevdg.networkkotlin.ui.details.DetailsFragment

@FragmentScope
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {

    fun inject(fragment: CompaniesFragment)
    fun inject(fragment: DetailsFragment)
}