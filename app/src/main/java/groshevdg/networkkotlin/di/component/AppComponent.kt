package groshevdg.networkkotlin.di.component

import dagger.Component
import groshevdg.networkkotlin.App
import groshevdg.networkkotlin.data.db.database.AppDatabase
import groshevdg.networkkotlin.data.network.StocksService
import groshevdg.networkkotlin.di.module.ContextModule
import groshevdg.networkkotlin.di.module.DatabaseModule
import groshevdg.networkkotlin.di.module.RetrofitModule
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitModule::class, ContextModule::class, DatabaseModule::class])
interface AppComponent {
    fun retrofit() : Retrofit
    fun stockService() : StocksService
    fun database() : AppDatabase

    fun inject(app: App)
}