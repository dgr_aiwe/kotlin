package groshevdg.networkkotlin.di.component

import dagger.Component
import groshevdg.networkkotlin.di.module.NavModule
import groshevdg.networkkotlin.di.module.ViewModelModule
import groshevdg.networkkotlin.di.scope.ActivityScope
import groshevdg.networkkotlin.ui.company.MainActivity

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [NavModule::class, ViewModelModule::class])
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)

    fun plusFragmentComponent() : FragmentComponent
}